
# Last update: 2015-09-14.20:52:18

#FUNCTIONS: xvt lsv lsvg lsco lscol lspr lsprl lsprf lsprfl sv ssv zeroversion makeorig makepred predversion myscript ccdiff ccdiff2

export GIT_SSL_NO_VERIFY=true
export http_proxy=http://wwwgate0.mot.com:1080 
export https_proxy=http://wwwgate0.mot.com:1080 

xvt() {
    grep -v ^Vtree ~/.xclearcase > ~/zzz.xclearcase
    cat > ~/.xclearcase << EOF
Vtree.width: 1169
Vtree.height: 970
Vtree.x: 25
Vtree.y: 25
EOF
    cat ~/zzz.xclearcase >>  ~/.xclearcase
    rm -f ~/zzz.xclearcase
    xlsvtree $*
}

export LESS='-S -n -e -j 5 -c -d -h 5 -i -M -q -x 4 -R'
export XLESS='-j5 -c -d -h 5 -i -M -q -x 4 -R'

uname_s=`uname -s`
for dir in /apps/public/gcc_3.3/sparc-sun-solaris2.8/lib /apps/public/gcc/sparc-sun-solaris2.9/lib $HOME/pfk/$PFKARCH/lib ; do
    if [[ -d $dir ]] ; then
        if [[ x$LD_LIBRARY_PATH = x ]] ; then
            LD_LIBRARY_PATH=$dir
        else
            LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$dir
        fi
    fi
done
export LD_LIBRARY_PATH

if [[ $uname_s = "Linux" ]] ; then
    if [[ -t 0 ]] ; then
	stty erase 0x7f
    fi
fi
unset uname_s

lsv() {
    typeset prefix
    if [[ $# -eq 0 ]] ; then
        prefix=${USER}_
    else
        prefix=$1
    fi
    ct lsview ${prefix}\* | cut -c3- | awk '{print $1}'
}

lsvg() {
    if [[ -z "$1" ]] ; then
        pattern=${USER}
    else
        pattern="$1"
    fi
    ct lsview | cut -c3- | grep "$pattern" | awk '{print $1}'
}

lspr() {
    ct lsprivate | grep -v '\[checkedout\]' | sort -r
}

lsprl() {
    ct lsprivate | grep -v checkedout | grep $PWD | sed -e s,$PWD/,, | sort -r
}

lsprf() {
    for f in $( lspr ) ; do
	if [[ -f $f ]] ; then
	    echo $f
	fi
    done
}

lsprfl() {
    for f in $( lsprl ) ; do
	if [[ -f $f ]] ; then
	    echo $f
	fi
    done
}

lsco() {
    ct lsco -cview -avobs -short | sort
}

lscol() {
    ct lsco -cview -avobs -short | grep $PWD | sed -e s,$PWD/,, | sort
}

sv() {
    ct setview $1
}

ssv() {
    select view in `lsvg $1` ; do
        sv $view
        break;
    done
}

setvobs() {
    export CLEARCASE_AVOBS=`grep -v ^# ~/.vobs | tr '\n' ':' | sed -e 's,:$,,'`
}

if __isksh ; then
    if [[ "x$CLEARCASE_CMDLINE" != "x" ]] ; then
       setvobs
    fi
else
    if [ "x$CLEARCASE_CMDLINE" != "x" ] ; then
       setvobs
    fi
fi

zeroversion() {
    ct ls $1 | 
     sed \
        -e 's,^\(.*\)  *Rule.*$,\1,'     \
        -e 's,^\(.*\)  *from.*$,\1,'     \
        -e 's,^\(.*/\).*$,\1,'          \
        -e 's,$,0,'
}

makeorig() {
    for f in $* ; do
        zv=`zeroversion $f`
        rm -f $f.orig
        echo $zv
        cp $zv $f.orig
    done
}

predversion() {
    ct describe $1 | grep ^version | sed -e 's,^.*\" from \([^ ]*\).*$,\1,'
}

makepred() {
    for f in $* ; do
        pv=`predversion $f`
        rm -f $f.orig
        echo $f@@$pv
        cp $f@@$pv $f.orig
    done
}

myscript() {
    logfile=$1
    shift
    MY_INSCRIPT=$HOME/$HOST.myscript.$$
    export MY_INSCRIPT
    echo rm -f $MY_INSCRIPT > $MY_INSCRIPT
    echo echo myscript started at `date` >> $MY_INSCRIPT
    echo "$@" >> $MY_INSCRIPT
    echo echo myscript done at `date` >> $MY_INSCRIPT
    script $logfile
    unset MY_INSCRIPT
}

efp() {
    invob=0

    if [[ x${1} != x${1#/} ]] ; then
        path=${1}
    else
        path=$PWD/${1}
    fi

    if [[ ${path#/vob} != $path ]] ; then
        invob=1
    elif [[ ${path#/projects} != $path ]] ; then
        invob=1
    fi

    if [[ ${invob} = 1 ]] ; then
        echo /ssh:pknaack1@`hostname`:${CLEARCASE_ROOT}${path}
    else
        echo /ssh:pknaack1@`hostname`:${path}
    fi
}

if [[ x$MY_INSCRIPT != x ]] ; then
    local_inscript=$MY_INSCRIPT
    unset MY_INSCRIPT
    sh $local_inscript
    rm -f $local_inscript
    exit 0
fi

ccdiff() {
    typeset file_arg
    typeset opts
    typeset ofile

    if [[ x$1 == x+g ]] ; then
        graphical=no
        shift
    else
        graphical=yes
    fi

    file_arg=$1
    shift
    opts=$1

    ofile=`zeroversion $file_arg`

    if [[ $graphical == no ]] ; then
        sd $file_arg $ofile
    else
        xsd $file_arg $ofile
    fi
}

ccdiff2() {
    typeset file_arg
    typeset opts
    typeset ofile

    if [[ x$1 == x+g ]] ; then
        graphical=no
        shift
    else
        graphical=yes
    fi

    file_arg=$1
    shift
    opts=$1

    ofile=`zeroversion $file_arg`

    if [[ $graphical == no ]] ; then
        sd2 $file_arg $ofile
    else
        xsd2 $file_arg $ofile
    fi
}

# always make sure my /home/$USER/bin is the first
# element of the path.
if [[ x${PATH#/home/${USER}/bin} == x$PATH ]] ; then
    __verbose fixing up busted path
    PATHhead=${PATH%:/home/${USER}*}
    PATHtail=${PATH#*${USER}/bin:*}
    PATH=/home/$USER/bin:$PATHhead:$PATHtail
    unset PATHhead
    unset PATHtail
    export PATH
fi

if [[ -f ${HOME}/.kshrc.local.mot.local ]] ; then
    . ${HOME}/.kshrc.local.mot.local
fi

# Local Variables:
# mode: Shell-script
# indent-tabs-mode: nil
# tab-width: 8
# eval: (add-hook 'write-file-hooks 'time-stamp)
# time-stamp-start: "Last update: "
# time-stamp-format: "%:y-%02m-%02d.%02H:%02M:%02S"
# time-stamp-end: "$"
# End:
