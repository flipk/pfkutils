
#define MAX_ARGS 20
#define CLEANUP_SCRIPT "/tmp/cleanup.sh"
#define MAX_LOG_FILES 5
#define LOGFILE_BASE "/tmp/bglog"
#define MAX_FILE_SIZE (1 * 1024 * 1024)
