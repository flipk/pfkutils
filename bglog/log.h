
void log_init(FILE *console_f);
void log_data(const char *buf, int len);
void log_periodic(void);
void log_finish(void);
