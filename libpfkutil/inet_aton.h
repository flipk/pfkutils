
#include "pfkutils_config.h"

#ifndef HAVE_INET_ATON
int inet_aton( register const char *cp, struct in_addr *addr );
#endif

